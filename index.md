---
layout: default
---

# Oh Hey - Welcome to my website

![](/assets/images/self1.jpeg)

My name is **Abhirup**, and I'm currently a physics PhD student living in India. My favourite things to do include watching seasonal anime, reading mystery novels and spending hours writing scripts to automate away minutes of work. Here's the obligatory [about me](/about/) page, if you want to know more.

## So, what exactly do I do?
I am a PhD student at the Indian Institute of Science Education and Research Kolkata, in India. I study strongly correlated fermionic systems, focusing on phase transitions of the Mott-Hubbard kind in the context of quantum impurity models. If you don't know what all that means, suffice it to say that I work in **theoretical condensed matter physics**. I have provided more information [here](/research/).

## Okay, but is there anything interesting here?
Take a look at this stuff.

- My thoughts on certain deep ideas of physics and nature.
- Using the power of scripting languages to do cool stuff.
- Do you like anime?

## Want to hear some miscellaneous news?
{% for post in site.posts %}
{% assign categories = post.categories | join:"," %}
{% if categories contains "update" %}
- {{ post.title }} [Learn More]({{ post.url }}){: .button }
{% endif %}
{% endfor %}
