---
layout: default
title: Updates
permalink: /updates/
---

{% assign latest_year = site.posts[0]["date"] | date: "%Y" %}
{% assign latest_month = site.posts[0]["date"] | date: "%m" | minus:6 %}
{% if latest_month <= 0 %}
{% assign month_class = "Jan - Jun" %}
{% else %}
{% assign month_class = "Aug - Dec" %}
{% endif %}

### {{ month_class }}, &nbsp;&nbsp;&nbsp;{{ latest_year }}
{% for post in site.posts %}
{% for cat in post.categories %}
{% if cat == "update" %}
{% assign month = post["date"] | date: "%m" | minus:6 %}
{% assign year = post["date"] | date: "%Y" %}
{% assign month_sign = latest_month | times: month %}
{% if month_sign < 0 or year != latest_year %}
{% assign latest_month = month %}
{% assign latest_year = year %}
{% if latest_month <= 0 %}
{% assign month_class = "Jan - Jun" %}
{% else %}
{% assign month_class = "Aug - Dec" %}
{% endif %}
### {{ month_class }}, &nbsp;&nbsp;&nbsp;{{ latest_year }}
{% endif %}
- {{ post.title }} [Learn More]({{ post.url }}){: .button }
{% break %}
{% endif %}
{% endfor %}
{% endfor %}
