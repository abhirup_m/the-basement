---
layout: default
title: Research Work
permalink: /work/
---

[Overview](#overview){: .button }
[Interests](#specific-interests){: .button }
[Ongoing Work](#ongoing-projects){: .button }
[Publications](#publications-and-preprints){: .button }
{: .button-set }

## Research overview

I work in theoretical condensed matter physics, more specifically in the subfield of **strongly correlated fermionic systems**. Strong correlation refers to interaction that cannot be expressed purely in terms of single-particle scattering, and must involve a large number of electrons behaving in a coordinated fashion. This roughly means that each electron will be able to "feel" the presence of other electrons, making this a highly non-trivial **many-body problem**.
I am a part of the Emergent Phenomena and Quantum Matter ([EPQM](https://www.iiserkol.ac.in/~slal/index.html)) group. The group's work involves studying quantum models of correlation and obtaining novel emergent states of matter.

## Specific interests

- Mechanisms of Kondo breakdown in **quantum impurity models** with particular emphasis on universal behaviour and **non-Fermi liquid physics** near such points critical points.

- **Mott metal-insulator transitions** in cuprates and other materials with localised orbitals, particularly from the vantage point of the physics of Kondo breakdown 

- Topological properties and entanglement features of gapped and gapless systems, particularly in the context of the **holographic principle** and gauge/gravity duality.

![](/assets/images/work/xkcd_areas_of_physics.jpg)

## Ongoing Projects

- We are presently working on a **new auxiliary model** approach towards studying strongly-correlated models. The approach leverages the "periodisation" ability of Bloch's theorem to translate results from simpler quantum impurity models to bulk lattice models.

- We are also probing the presence of a $$T=0$$ quantum phase transition of a **Kondo model when placed in a local magnetic field**; the deeper goal is to study the effects of decoherence (here, the interaction of the Kondo impurity with the conduction bath) on a qubit in the presence of a measurement mechanism (the magnetic field).

- As a third project, we are analysing an **extended impurity model that involves two conduction bath layers**. We are trying to discover a mechanism by which to remove the coexistence region that is generically seen in DMFT results, and hence reveal the quantum critical point at finite temperatures.

## Publications and Preprints

{% for work in site.data.publications %}
{% if work[1]["doi"] %}
- {{ work[0] }} <br>
[Learn More]({{ work[1]["permalink"] }}){: .button } [Journal Ref.]({{ work[1]["doi"] }}){: .button } [Arxiv Ref.]({{ work[1]["arxiv_doi"] }}){: .button }
{% else %}
- {{ work[0] }} <br>
[Learn More]({{ work[1]["permalink"] }}){: .button } [Arxiv Ref.]({{ work[1]["arxiv_doi"] }}){: .button }
{% endif %}
{% endfor %}

