---
layout: default
title: "All About Yours Truly"
permalink: /about/
---

![](/assets/images/self3.jpeg)

I am Abhirup Mukherjee, a research scholar at the Indian Institute of Science Education and Research Kolkata [(IISER Kolkata)](https://www.iiserkol.ac.in/), living in Kalyani, West Bengal, India.
I work in theoretical condensed matter physics, and am a part of the Emergent Phenomena and Quantum Matter ([EPQM](https://www.iiserkol.ac.in/~slal/index.html)) group.
I study various topics in the field of strongly correlated materials.


When I am not working, I enjoy catching up on the seasonal [anime](https://myanimelist.net/featured/1382/What_is_Anime). Although I am not particularly picky when it comes to the genres of the anime I watch, I do enjoy [slice of life](https://en.wikipedia.org/wiki/Slice_of_life#:~:text=Slice%20of%20life%20anime%20and%20manga%20are%20narratives%20%22without%20fantastical,ties%20with%20the%20characters.%22%20The) shows and thrillers the most. From time to time, I also pick up crime novels. Whenever I get some time, I dabble in [static site generators](https://en.wikipedia.org/wiki/Jekyll_(software)),  Python and shell scripting, and try out new Linux distributions - I am quite passionate about [ricing my desktop]("https://www.reddit.com/r/unixporn/wiki/themeing/dictionary#wiki_rice").

![](/assets/images/levi_kenny.gif)
_Key animation of the Levi vs Kenny-squad scene by Arifumi Imai, in the anime Attack on Titan_

I used to love watching and playing football during my school years. Nowadays I am mostly a seasonal fan.
I am part of a group named [Projectyl](https://projectyl.github.io/) that helps undergraduate and graduate students in finding and applying for short projects and internships in India, with the goal of making it easier for them to transition into their research programs. Feel free to check out our [work](https://projectyl.github.io/) if you are interested in landing a summer or winter project in physics. Hit me up if you want to be a part of the group.

## Contact information

[󰇮](mailto:mukherjeeabhirup44@gmail.com)
[](https://www.facebook.com/Seary.Blue)
[󰌻](https://www.linkedin.com/in/abhirup-mukherjee-665588229)
[](https://github.com/abhirup-m)
{: .contact-icons }

You can always send me [emails](mailto:mukherjeeabhirup44@gmail.com)  for any sort of communication.

You can also reach out to me on [Facebook](https://www.facebook.com/Seary.Blue)  or [LinkedIn](https://www.linkedin.com/in/abhirup-mukherjee-665588229).

Feel free to check out my hobby projects and my PhD research work on [Github](https://github.com/abhirup-m).
